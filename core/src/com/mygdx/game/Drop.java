package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

import java.awt.Color;

public class Drop extends Game {

	public GameScreen gameScreen;
	public MainMenuScreen mainMenuScreen;
	public SettingsScreen settingsScreen;

  	SpriteBatch batch;
	BitmapFont mainMenuFont, gameScreenFont;

	public void create() {
		gameScreen = new GameScreen(this);
		mainMenuScreen = new MainMenuScreen(this);
		settingsScreen = new SettingsScreen(this);

		batch = new SpriteBatch();

		mainMenuFont = new BitmapFont();
		gameScreenFont = new BitmapFont();

		FreeTypeFontGenerator gen = new FreeTypeFontGenerator(Gdx.files.internal("font.ttf"));
		FreeTypeFontGenerator.FreeTypeFontParameter p = new FreeTypeFontGenerator.FreeTypeFontParameter();
		p.borderColor = com.badlogic.gdx.graphics.Color.BLACK;
		p.borderWidth = 4;
		p.color = com.badlogic.gdx.graphics.Color.CYAN;
		p.size = 24;

		mainMenuFont = gen.generateFont(p);

		gen = new FreeTypeFontGenerator(Gdx.files.internal("game_screen_font.ttf"));
		p.borderColor = com.badlogic.gdx.graphics.Color.BLACK;
		p.borderWidth = 3;
		p.color = com.badlogic.gdx.graphics.Color.SKY;
		p.size = 42;

		gameScreenFont = gen.generateFont(p);


		this.setScreen(mainMenuScreen);
	}

	public void render() {
		super.render(); // important!
	}

	public void dispose() {
		batch.dispose();
		mainMenuFont.dispose();
	}

}
