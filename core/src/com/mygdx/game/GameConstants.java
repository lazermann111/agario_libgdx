package com.mygdx.game;



public class GameConstants
{

    public static final float CAMERA_ZOOM = 1.5f;
    public static final int SCREEN_WIDTH = 900;
    public static final int SCREEN_HEIGHT = 900;

    public static final int WORLD_WIDTH = 200000;
    public static final int WORLD_HEIGHT = 200000;


    public static final int RAINDROP_SIZE = 64;
    public static final int RAINDROP_ACCELERATION = 200;
    public static final int RAINDROP_SPAWN_AREA = 1000;
    public static final int RAINDROP_TTL = 10*1000; // 10 sec


    public static final float PLAYER_MOUSE_ACCELERATION_RATIO = 200f;
    public static final float PLAYER_SPEED_LIMIT = 40;
    public static final int PLAYER_SIZE = 64;
    public static final int PLAYER_KEYBOARD_ACCELERATION = 300;

    public static final int GRID_DENSITY = 10;


    public static final boolean DEBUG_ENABLED = true;
}
