package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.Iterator;

import static com.mygdx.game.GameConstants.CAMERA_ZOOM;
import static com.mygdx.game.GameConstants.GRID_DENSITY;
import static com.mygdx.game.GameConstants.PLAYER_KEYBOARD_ACCELERATION;
import static com.mygdx.game.GameConstants.PLAYER_MOUSE_ACCELERATION_RATIO;
import static com.mygdx.game.GameConstants.PLAYER_SIZE;
import static com.mygdx.game.GameConstants.PLAYER_SPEED_LIMIT;
import static com.mygdx.game.GameConstants.RAINDROP_SPAWN_AREA;
import static com.mygdx.game.GameConstants.RAINDROP_TTL;
import static com.mygdx.game.GameConstants.SCREEN_HEIGHT;
import static com.mygdx.game.GameConstants.SCREEN_WIDTH;

public class GameScreen implements Screen {
	final Drop game;

	private Texture dropImage;
	private Texture player_image;
	private Sound dropSound;
	private Music music;
	private OrthographicCamera camera;
	private Rectangle player;
	private ShapeRenderer shapeRenderer = new ShapeRenderer();


	private Array<Raindrop> raindrops;
	private long lastDropTime;
	private int dropsGathered;

	// todo add minimap with player pos in world
	// todo add input processor for handling mousewheel (zooming)
	// todo add moving parallax background


	public GameScreen(final Drop gam) {
		this.game = gam;
		shapeRenderer.setAutoShapeType(true);
		// load the images for the droplet and the player, 64x64 pixels each
		dropImage = new Texture(Gdx.files.internal("player_circle.png"));
		player_image = new Texture(Gdx.files.internal("player_circle.png"));

		// load the drop sound effect and the rain background "music"
		dropSound = Gdx.audio.newSound(Gdx.files.internal("drop.wav"));
		music = Gdx.audio.newMusic(Gdx.files.internal("interstellar.mp3"));
		music.setLooping(true);

		// create the camera and the SpriteBatch
		camera = new OrthographicCamera();
		camera.setToOrtho(false, SCREEN_WIDTH, SCREEN_HEIGHT);
		// tell the camera to update its matrices.
		camera.zoom = CAMERA_ZOOM;

		// create a Rectangle to logically represent the player
		player = new Rectangle();
		player.x = 0;
		player.y = 0;
		player.width = PLAYER_SIZE;
		player.height = PLAYER_SIZE;

		// create the raindrops array and spawn the first raindrop
		raindrops = new Array<Raindrop>();
		spawnRaindrop();

	}

	private void spawnRaindrop() {
		Raindrop raindrop = new Raindrop(dropImage);

		raindrop.body = new Rectangle();
		raindrop.body.x =  (MathUtils.random(player.x - RAINDROP_SPAWN_AREA, player.x + RAINDROP_SPAWN_AREA));
		raindrop.body.y =(MathUtils.random(player.y - RAINDROP_SPAWN_AREA, player.y + RAINDROP_SPAWN_AREA));
		raindrop.body.width = raindrop.diameter;
		raindrop.body.height = raindrop.diameter;
		raindrop.spawnTime = System.currentTimeMillis();
		raindrops.add(raindrop);
		lastDropTime = TimeUtils.nanoTime();
	}

	@Override
	public void render(float delta) {
		// clear the screen with a white color. The
		// arguments to glClearColor are the red, green
		// blue and alpha component in the range [0,1]
		// of the color to be used to clear the screen.
		Gdx.gl.glClearColor(game.settingsScreen.r, game.settingsScreen.g, game.settingsScreen.b, game.settingsScreen.a);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		//center camera to player
		camera.position.set(player.getX() + (player.width/2), player.getY() + (player.height/2), 0);

		// tell the SpriteBatch to render in the
		// coordinate system specified by the camera.
		game.batch.setProjectionMatrix(camera.combined);

		// begin a new batch and draw the player and
		// all drops
		drawAllObjects();

		// process user input
		processUserInput();
		camera.update();
		// check if we need to create a new raindrop
		if (TimeUtils.nanoTime() - lastDropTime > 1000000000)
			spawnRaindrop();

		// move the raindrops, remove any that are beneath the bottom edge of
		// the screen or that hit the player. In the later case we play back
		// a sound effect as well.
		Iterator<Raindrop> iter = raindrops.iterator();
		while (iter.hasNext())
		{
			Raindrop raindrop = iter.next();
			//raindrop.body.y -= RAINDROP_ACCELERATION * Gdx.graphics.getDeltaTime();
			if ((System.currentTimeMillis() - raindrop.spawnTime) > RAINDROP_TTL)
			{
				iter.remove();
			}
			if (raindrop.getBody().overlaps(player)) {
				dropsGathered++;
				dropSound.play();
				player.width *= 1.05;
				player.height *= 1.05;
				if (dropsGathered % 5 == 0)
				{
					zoomingInProgress = true;
					finalZoom = (float) (camera.zoom + 0.2);
				}
				iter.remove();
			}
		}
		processZoom(delta);
		Logz();

		if (Gdx.input.isKeyPressed(Input.Keys.ESCAPE)) {
			game.setScreen(new SettingsScreen(game));
		}
	}

	boolean zoomingInProgress = false;
	float finalZoom;

	private void processZoom(float delta){
		if(!zoomingInProgress)return;

		System.out.println("camera.zoom =" + camera.zoom);
		camera.zoom *= (1+delta/2);
		if(camera.zoom >= finalZoom) zoomingInProgress = false;
	}


	private void drawAllObjects(){

		game.batch.begin();
		game.gameScreenFont.draw(game.batch, "Collected: " + dropsGathered,
				player.x - player.width/2 + SCREEN_WIDTH/2, player.y + player.height + SCREEN_HEIGHT/2); //todo very hacky way based on knowledge that player is always in center of cam
		game.batch.setColor(Color.CYAN);
		game.batch.draw(player_image, player.x, player.y, player.width, player.height);
		game.batch.setColor(1,1,1,1);

		for (Raindrop raindrop : raindrops)	{
			//todo maybe draw only visible drops

			game.batch.setColor(raindrop.r,raindrop.g, raindrop.b, raindrop.a);
			game.batch.draw(raindrop.getTexture(), raindrop.body.x, raindrop.body.y, raindrop.diameter, raindrop.diameter);
			game.batch.setColor(1,1,1,1);

		}
		game.batch.end();
		shapeRenderer.setColor(Color.BLACK);

		shapeRenderer.begin(ShapeRenderer.ShapeType.Line);

		for (int i = 0; i < player.width; i+= GRID_DENSITY)
		{
			shapeRenderer.line(i, player.y + player.height, 33, i, player.y - player.height,33 );

		}
		shapeRenderer.end();
	}

	private void processUserInput()	{
		//if (Gdx.input.isTouched())
		{
			acceleratePlayer();
		}
		if (Gdx.input.isKeyPressed(Input.Keys.LEFT))
			player.x -= PLAYER_KEYBOARD_ACCELERATION * Gdx.graphics.getDeltaTime();
		if (Gdx.input.isKeyPressed(Input.Keys.RIGHT))
			player.x += PLAYER_KEYBOARD_ACCELERATION * Gdx.graphics.getDeltaTime();
		if (Gdx.input.isKeyPressed(Input.Keys.UP))
			player.y += PLAYER_KEYBOARD_ACCELERATION * Gdx.graphics.getDeltaTime();
		if (Gdx.input.isKeyPressed(Input.Keys.DOWN))
			player.y -= PLAYER_KEYBOARD_ACCELERATION * Gdx.graphics.getDeltaTime();
		if (Gdx.input.isKeyPressed(Input.Keys.R))
			player.setPosition(0,0);

	}

	private void acceleratePlayer() {
		Vector3 touchPos = new Vector3();
		touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
		camera.unproject(touchPos);


		float deltaX = (player.x - touchPos.x);
		float deltaY = (player.y - touchPos.y);

		if(deltaX > 0)
			player.x -= Math.min(PLAYER_SPEED_LIMIT, deltaX / PLAYER_MOUSE_ACCELERATION_RATIO);
		if(deltaX < 0)
			player.x -= Math.max(- PLAYER_SPEED_LIMIT, deltaX / PLAYER_MOUSE_ACCELERATION_RATIO);

		if(deltaY > 0)
			player.y -= Math.min(PLAYER_SPEED_LIMIT, deltaY / PLAYER_MOUSE_ACCELERATION_RATIO);

		if(deltaY < 0)
			player.y -= Math.max(- PLAYER_SPEED_LIMIT, deltaY / PLAYER_MOUSE_ACCELERATION_RATIO);
	}

	private void Logz()	{}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void show() {
		// start the playback of the background music
		// when the screen is shown
		music.play();
	}

	@Override
	public void hide() {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose() {
		dropImage.dispose();
		player_image.dispose();
		dropSound.dispose();
		music.dispose();
	}
}