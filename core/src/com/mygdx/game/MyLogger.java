package com.mygdx.game;


import static com.mygdx.game.GameConstants.DEBUG_ENABLED;

public  class MyLogger
{
   public static void Log(String msg)
   {
       if(!DEBUG_ENABLED) return;
       System.out.println(msg);
   }
}
