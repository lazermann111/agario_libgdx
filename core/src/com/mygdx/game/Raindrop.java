package com.mygdx.game;


import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;

public class Raindrop
{
    public Texture texture;
    public Rectangle body;
    public long spawnTime;
    public float r, g, b, a;
    public int diameter;
    //public float x,y;

    public Raindrop(Texture tex){

        this.texture = tex;

        this.r = MathUtils.random(0.0f,1.0f);
        this.g = MathUtils.random(0.0f,1.0f);
        this.b = MathUtils.random(0.0f,1.0f);
        this.a = MathUtils.random(1f);

        this.diameter = MathUtils.random(16,128);
    }

    public Texture getTexture() {
        return texture;
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
    }

    public Rectangle getBody() {
        return body;
    }

    public void setBody(Rectangle body) {
        this.body = body;
    }


}
