package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;

public class SettingsScreen implements Screen {

    final Drop game;
    OrthographicCamera camera;
    Texture background;



    public int musicLevel;
    public int effectsLevel;
    public int speedLevel;
    public float r, g, b =1, a;

    public SettingsScreen(final Drop gam){
        game = gam;

        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);

        background = new Texture("background.jpg");

        this.musicLevel = 100;
        this.effectsLevel = 100;
        this.speedLevel = 50;
        this.r = 1;
        this.g = 1;
        this.b = 1;
        this.a = 1;
    }


    @Override
    public void show() {
        
    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        //todo add actual GUI

        camera.update();
        game.batch.setProjectionMatrix(camera.combined);

        game.batch.begin();
        game.batch.draw(background, 0, 0, GameConstants.SCREEN_WIDTH, GameConstants.SCREEN_HEIGHT);
        game.batch.end();

        if (Gdx.input.isTouched()) {
            game.setScreen(game.gameScreen);
            dispose();
        }

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
